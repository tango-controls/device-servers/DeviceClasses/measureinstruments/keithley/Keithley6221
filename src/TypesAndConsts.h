//=============================================================================
// TypesAndConsts.h
//=============================================================================
// abstraction.......Definitions
// class.............TypesAndConsts 
// original author...SGARA NEXEYA
//=============================================================================
#ifndef _TYPES_AND_CONSTS_S
#define _TYPES_AND_CONSTS_S

namespace Keithley6221_ns
{
	//- readable range
#define RR100MA "100mA"
#define RR20MA "20mA"
#define RR2MA "2mA"
#define RR200UA "200�A"
#define RR20UA "20�A"
#define RR2UA "2�A"
#define RR200NA "200nA"
#define RR20NA "20nA"
#define RR2NA "2nA"

	//- read range
#define R100MA "1.000000E-01"
#define R20MA "2.000000E-02"
#define R2MA "2.000000E-03"
#define R200UA "2.000000E-04"
#define R20UA "2.000000E-05"
#define R2UA "2.000000E-06"
#define R200NA "2.000000E-07"
#define R20NA "2.000000E-08"
#define R2NA "2.000000E-09"

	//- set range
#define SETRANGE100MA "1.000000E-01"
#define SETRANGE20MA "2.000000E-02"
#define SETRANGE2MA "2.000000E-03"
#define SETRANGE200UA "2.000000E-04"
#define SETRANGE20UA "2.000000E-05"
#define SETRANGE2UA "2.000000E-06"
#define SETRANGE200NA "2.000000E-07"
#define SETRANGE20NA "2.000000E-08"
#define SETRANGE2NA "2.000000E-09"

	//- internal range
#define COEFFRANGEMA 1000
#define COEFFRANGEUA 1000000
#define COEFFRANGENA 1000000000
}

#endif // _TYPES_AND_CONSTS_S

