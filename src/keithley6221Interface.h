//=============================================================================
// keithley6221Interface.h
//=============================================================================
// abstraction.......This class aims to send commands to a a keithley 6221
// class.............keithley6221Interface 
// original author...SGARA NEXEYA
//=============================================================================
#ifndef _KEITHLEY6221INTERFACE_H
#define _KEITHLEY6221INTERFACE_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat/utils/String.h>
#include <yat4tango/InnerAppender.h>

namespace Keithley6221_ns
{
	// ============================================================================
	// class: keithley6221Interface
	// ============================================================================
	class keithley6221Interface
	{
	public:
		//- ctor
		keithley6221Interface(Tango::DeviceProxy * p_dev_proxy);

		//- dtor
		~keithley6221Interface();
		
    //- read *IDN? for checking if the device is connected
		std::string read_id() throw (Tango::DevFailed);
		
    //-read_autorange
		bool read_autorange() throw (Tango::DevFailed);
		
    //-write_autorange
		void write_autorange(bool p_state) throw (Tango::DevFailed);
		
    //-read_range
		std::string read_range() throw (Tango::DevFailed);
		
    //- write range
		void write_range(std::string p_range) throw (Tango::DevFailed);
		
    //-write_current
		void write_current(double p_current) throw (Tango::DevFailed);
		
    //-read_current
		double read_current() throw (Tango::DevFailed);
		
    //-write_compliance
		void write_compliance(double p_compliance) throw (Tango::DevFailed);
		
    //-read_measurement_status
		int read_measurement_status() throw (Tango::DevFailed);
		
    //-read_errors
		std::string read_errors() throw (Tango::DevFailed);
		
    //-read_output_state
		bool read_output_state() throw (Tango::DevFailed);
		
    //-on
		void on() throw (Tango::DevFailed);
		
    //-off
		void off() throw (Tango::DevFailed);
		
    //- get the range on a readable form from the set value
		std::string get_readable_range_from_set(std::string p_str_in);
		
    //- get the coefficient for range scale
		int get_coeff_for_range(std::string p_str_in);

	protected:
		//- get the range on a readable form
		std::string get_readable_range(std::string p_str_in);
	
  private:
		Tango::DeviceProxy * m_deviceProxy;
	};
}
#endif