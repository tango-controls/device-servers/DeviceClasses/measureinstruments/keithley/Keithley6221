//=============================================================================
//keithley6221Interface.cpp
//=============================================================================
// abstraction.......This class aims to send commands to a keithley 6221
// class.............keithley6221Interface
// original author...SGARA NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES 
// ============================================================================

#include <keithley6221Interface.h>
#include <TypesAndConsts.h>

namespace Keithley6221_ns
{

	//-----------------------------------------------------------------------------
	//- Check Device Proxy macro:
#define CHECK_PROXY \
	if (! this->m_deviceProxy) \
	{ \
	THROW_DEVFAILED(_CPTC("INTERNAL_ERROR"), \
	_CPTC("request aborted - the Device Proxy isn't properly initialized"), \
	_CPTC("keithley6221Interface::check_proxy")); \
}

	//*****************************************************************************
	// keithley6221Interface
	//*****************************************************************************
	// ============================================================================
	// keithley6221Interface::keithley6221Interface
	// ============================================================================
	keithley6221Interface::keithley6221Interface(Tango::DeviceProxy * p_dev_proxy)
	{
		m_deviceProxy = p_dev_proxy;
	}
	// ============================================================================
	// keithley6221Interface::~keithley6221Interface
	// ============================================================================
	keithley6221Interface::~keithley6221Interface()
	{

	}

	// ============================================================================
	// keithley6221Interface::read_id
	// ============================================================================
	std::string keithley6221Interface::read_id() 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		yat::String l_str_to_trim;
		l_str = "*IDN?\n";
		string l_string;

		l_data << l_str;

		try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::read_id"));
		}

		l_data_ret >> l_string;
		l_str_to_trim = l_string.data();
		l_str_to_trim.trim();

		return l_str_to_trim;
	}	

	// ============================================================================
	// keithley6221Interface::read_autorange
	// ============================================================================
	bool keithley6221Interface::read_autorange() 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		bool l_ret = false;
		Tango::DeviceData l_data;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		string l_string;
		l_str = "SOURCE:CURRENT:RANGE:AUTO?\n";

		l_data << l_str;

		try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::read_autorange"));
		}

		l_data_ret >> l_string;
		int l_res = atoi(l_string.data());

		if (l_res == 1)
		{
			l_ret = true;
		}

		return l_ret;
	}

	// ============================================================================
	// keithley6221Interface::write_autorange
	// ============================================================================
	void keithley6221Interface::write_autorange(bool p_state) 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str;

		if (p_state)
		{
			l_str = "SOURCE:CURRENT:RANGE:AUTO ON\n";
		} 
		else
		{
			l_str = "SOURCE:CURRENT:RANGE:AUTO OFF\n";
		}

		l_data << l_str;

		try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::write_autorange"));
		}
	}

	// ============================================================================
	// keithley6221Interface::read_range
	// ============================================================================
	std::string keithley6221Interface::read_range() 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		yat::String l_str_to_trim;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		l_str = "SOURCE:CURRENT:RANGE?\n";
		string l_string;

		l_data << l_str;

		try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::read_range"));
		}

		l_data_ret >> l_string;
		l_str_to_trim = l_string.data();
		l_str_to_trim.trim();
		l_str_to_trim = get_readable_range(l_str_to_trim);

		return l_str_to_trim;
	}

	// ============================================================================
	// keithley6221Interface::write_range
	// ============================================================================
	void keithley6221Interface::write_range(std::string p_range) 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str;
		char l_char[50] = "SOURCE:CURRENT:RANGE ";
		l_str = Tango::string_dup(strcat(l_char, p_range.data()));
		l_str = Tango::string_dup(strcat(l_str, "\n"));

		l_data << l_str;

		try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::write_range"));
		}
	}

	// ============================================================================
	// keithley6221Interface::write_current
	// ============================================================================
	void keithley6221Interface::write_current(double p_current) 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str;
		char l_char[50] = "";
		sprintf(l_char, "SOURCE:CURRENT %e\n", p_current);
		l_str = Tango::string_dup(l_char);

		l_data << l_str;

		try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::write_current"));
		}
	}

	// ============================================================================
	// keithley6221Interface::read_current
	// ============================================================================
	double keithley6221Interface::read_current() 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		yat::String l_str_to_trim;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		l_str = "SOURCE:CURRENT?\n";
		string l_string;

		l_data << l_str;

		try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::read_range"));
		}

		l_data_ret >> l_string;
		l_str_to_trim = l_string.data();
		l_str_to_trim.trim();
		double l_val = atof(l_str_to_trim.data());

		return l_val;
	}

	// ============================================================================
	// keithley6221Interface::write_compliance
	// ============================================================================
	void keithley6221Interface::write_compliance(double p_compliance) 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str;
		char l_char[50] = "";
		sprintf(l_char, "SOURCE:CURRENT:COMPLIANCE %.2f\n", p_compliance);
		l_str = Tango::string_dup(l_char);

		l_data << l_str;

		try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::write_compliance"));
		}
	}

	// ============================================================================
	// keithley6221Interface::read_measurement_status
	// ============================================================================
	int keithley6221Interface::read_measurement_status() 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		yat::String l_str_to_trim;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		l_str = "STAT:MEAS:COND?\n";
		string l_string;

		l_data << l_str;

		try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::read_range"));
		}

		l_data_ret >> l_string;
		l_str_to_trim = l_string.data();
		l_str_to_trim.trim();
		int l_res = atoi(l_str_to_trim.c_str());

		return l_res;
	}

	// ============================================================================
	// keithley6221Interface::read_errors
	// ============================================================================
	std::string keithley6221Interface::read_errors() 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		yat::String l_str_to_trim;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		l_str = "STATUS:QUEUE?\n";
		string l_string;

		l_data << l_str;

		try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::read_errors"));
		}

		l_data_ret >> l_string;
		l_str_to_trim = l_string.data();
		l_str_to_trim.trim();

		return l_str_to_trim;
	}

	// ============================================================================
	// keithley6221Interface::read_output_state
	// ============================================================================
	bool keithley6221Interface::read_output_state() 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		bool l_ret = false;
		Tango::DeviceData l_data;
		Tango::DeviceData l_data_ret;
		Tango::DevString l_str;
		string l_string;
		l_str = "OUTPUT:STATE?\n";

		l_data << l_str;

		try
		{
			l_data_ret = m_deviceProxy->command_inout("WriteRead",l_data);
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::read_output_state"));
		}

		l_data_ret >> l_string;
		int l_res = atoi(l_string.data());

		if (l_res == 1)
		{
			l_ret = true;
		}
		
		return l_ret;
	}

	// ============================================================================
	// keithley6221Interface::on
	// ============================================================================
	void keithley6221Interface::on() 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str;
		l_str = "OUTPUT:STATE ON\n";

		l_data << l_str;

		try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::on"));
		}
	}


	// ============================================================================
	// keithley6221Interface::off
	// ============================================================================
	void keithley6221Interface::off() 
		throw (Tango::DevFailed)
	{
		CHECK_PROXY;

		Tango::DeviceData l_data;
		Tango::DevString l_str;
		l_str = "OUTPUT:STATE OFF\n";

		l_data << l_str;

		try
		{
			m_deviceProxy->command_inout("Write",l_data );
		}
		catch (...)
		{
			THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
				_CPTC("Problem communicating with the socket device"),
				_CPTC("keithley6221Interface::off"));
		}
	}

	// ============================================================================
	// keithley6221Interface::get_readable_range
	// ============================================================================
	std::string keithley6221Interface::get_readable_range(std::string p_str_in)
	{
		std::string	argout;
		if (strcmp(p_str_in.data(),R2NA) == 0)
		{
			argout = RR2NA;
		}
		if (strcmp(p_str_in.data(),R20NA) == 0)
		{
			argout = RR20NA;
		}
		if (strcmp(p_str_in.data(),R200NA) == 0)
		{
			argout = RR200NA;
		}
		if (strcmp(p_str_in.data(),R2UA) == 0)
		{
			argout = RR2UA;
		}
		if (strcmp(p_str_in.data(),R20UA) == 0)
		{
			argout = RR20UA;
		}
		if (strcmp(p_str_in.data(),R200UA) == 0)
		{
			argout = RR200UA;
		}
		if (strcmp(p_str_in.data(),R2MA) == 0)
		{
			argout = RR2MA;
		}
		if (strcmp(p_str_in.data(),R20MA) == 0)
		{
			argout = RR20MA;
		}
		if (strcmp(p_str_in.data(),R100MA) == 0)
		{
			argout = RR100MA;
		}
		return argout;
	}

	// ============================================================================
	// keithley6221Interface::get_readable_range_from_set
	// ============================================================================
	std::string keithley6221Interface::get_readable_range_from_set(std::string p_str_in)
	{
		std::string	argout;
		if (strcmp(p_str_in.data(),SETRANGE2NA) == 0)
		{
			argout = RR2NA;
		}
		if (strcmp(p_str_in.data(),SETRANGE20NA) == 0)
		{
			argout = RR20NA;
		}
		if (strcmp(p_str_in.data(),SETRANGE200NA) == 0)
		{
			argout = RR200NA;
		}
		if (strcmp(p_str_in.data(),SETRANGE2UA) == 0)
		{
			argout = RR2UA;
		}
		if (strcmp(p_str_in.data(),SETRANGE20UA) == 0)
		{
			argout = RR20UA;
		}
		if (strcmp(p_str_in.data(),SETRANGE200UA) == 0)
		{
			argout = RR200UA;
		}
		if (strcmp(p_str_in.data(),SETRANGE2MA) == 0)
		{
			argout = RR2MA;
		}
		if (strcmp(p_str_in.data(),SETRANGE20MA) == 0)
		{
			argout = RR20MA;
		}
		if (strcmp(p_str_in.data(),SETRANGE100MA) == 0)
		{
			argout = RR100MA;
		}
		return argout;
	}

	// ============================================================================
	// keithley6221Interface::get_coeff_for_range
	// ============================================================================
	int keithley6221Interface::get_coeff_for_range(std::string p_str_in)
	{
		int	argout;
		if (strcmp(p_str_in.data(),RR2NA) == 0)
		{
			argout = COEFFRANGENA;
		}
		if (strcmp(p_str_in.data(),RR20NA) == 0)
		{
			argout = COEFFRANGENA;
		}
		if (strcmp(p_str_in.data(),RR200NA) == 0)
		{
			argout = COEFFRANGENA;
		}
		if (strcmp(p_str_in.data(),RR2UA) == 0)
		{
			argout = COEFFRANGEUA;
		}
		if (strcmp(p_str_in.data(),RR20UA) == 0)
		{
			argout = COEFFRANGEUA;
		}
		if (strcmp(p_str_in.data(),RR200UA) == 0)
		{
			argout = COEFFRANGEUA;
		}
		if (strcmp(p_str_in.data(),RR2MA) == 0)
		{
			argout = COEFFRANGEMA;
		}
		if (strcmp(p_str_in.data(),RR20MA) == 0)
		{
			argout = COEFFRANGEMA;
		}
		if (strcmp(p_str_in.data(),RR100MA) == 0)
		{
			argout = COEFFRANGEMA;
		}
		return argout;
	}
}// namespace Keithley6221_ns